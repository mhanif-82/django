from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')

def aboutme(request):
    return render(request, 'aboutme.html')

def myhooby(request):
    return render(request, 'myhobby.html')

def contact(request):
    return render(request, 'contact.html')

def resume(request):
    return render(request, 'resume.html')



